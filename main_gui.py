import tkinter as tk


def test_function():
    print("It Works!")
    print(e1_value.get())
    float_variable = float(e1_value.get())
    t1.insert(tk.END, str(float_variable))
    t1.insert(tk.END, "\n")


window = tk.Tk()

b1 = tk.Button(window, text="Train", width=20, command=test_function)
b1.grid(row=0, column=0)

e1_value = tk.StringVar()
e1 = tk.Entry(window, textvariable=e1_value)
e1.grid(row=0, column=1)

t1 = tk.Text(window)
t1.grid(row=1, column=0, columnspan=2)

window.mainloop()
