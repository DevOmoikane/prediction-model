import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from sklearn import linear_model
import joblib
from tkinter import *
import sqlite3
import random


column_names = ['sprint', 'duration', 'team', 'name', 'seniority', 'val_seniority', 'ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce', 'ua1', 'ua2', 'ua3', 'ua5', 'ua8', 'ua13', 'ca', 'e']
personal_data_columns = ['sprint', 'val_seniority', 'ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce', 'ua1', 'ua2', 'ua3', 'ua5', 'ua8', 'ua13', 'ca', 'e']
sprint_data_columns = ['ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce', 'ua1', 'ua2', 'ua3', 'ua5', 'ua8', 'ua13', 'ca', 'e']
data_in_columns = ['val_seniority', 'ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce']
sprint_in_columns = ['ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce']
result_column = 'e'

class DataLoader:

    def load_data_file(self, data_file):
        self.data_file = data_file
        temp_data = pd.read_excel(data_file, None)
        pages = temp_data.keys()
        self.pages = []
        self.data = {}
        for page in pages:
            try:
                data = pd.read_excel(data_file, page, na_values=[0], names=column_names)
                self.data[page] = data.fillna(value=0)
                self.pages.append(page)
            except:
                None
            finally:
                None

    def get_pages(self):
        return self.pages

    def get_team_data(self, team):
        return_data = self.data[team]
        return_data = return_data[personal_data_columns]
        return return_data

    def get_team_data_by_sprint(self, team):
        team_data = self.data[team]
        sprint_list = self.get_sprint_list(team)
        return_data = []
        for sprint in sprint_list:
            sprint_data = team_data[team_data['sprint'] == sprint]
            sprint_data = sprint_data[sprint_data_columns]
            sum_sprint = sprint_data.sum(axis=0)
            sum_sprint['e'] = sum_sprint['ca'] / sum_sprint['ce']
            return_data.append(sum_sprint)
        return return_data

    def get_sprint_list(self, team):
        team_data = self.data[team]
        sprints = team_data[['sprint']]
        sprint_list = list(set(sprints['sprint']))
        return sprint_list

    def get_team_sprint_data(self, team, sprint):
        team_data = self.data[team]
        sprint_data = team_data[team_data['sprint'] == sprint]
        sprint_data = sprint_data[personal_data_columns]
        return sprint_data

    def get_team_data_last_sprint(self, team):
        sprint_list = self.get_sprint_list(team)
        return self.get_team_sprint_data(team, sprint_list[-1])


class Predictor:
    dataFile = ""

    def __init__(self):
        self.clean_data()

    def clean_data(self):
        self.data_file = ""
        self.predictors = {}

    def load_data(self, data_file):
        self.data_file = data_file
        self.data_loader = DataLoader()
        self.data_loader.load_data_file(data_file)

    def get_teams(self):
        return self.data_loader.get_pages()

    def train_team(self, team, n_estimators=10, max_depth=5, omit_last_sprint=False):
        clf = RandomForestRegressor(n_estimators=n_estimators, max_depth=max_depth, random_state=0)
        team_data = self.data_loader.get_team_data(team)
        if omit_last_sprint:
            sprint_list = self.data_loader.get_sprint_list(team)
            last_sprint = sprint_list[-1]
            team_data = team_data[team_data['sprint'] != last_sprint]
        train_d = team_data[data_in_columns]
        train_t = team_data[result_column]
        clf.fit(train_d, train_t)
        return clf

    def train_team_by_sprint(self, team, n_estimators=10, max_depth=5):
        clf = RandomForestRegressor(n_estimators=n_estimators, max_depth=max_depth, random_state=0)
        sprint_data = self.data_loader.get_team_data_by_sprint(team)
        train_d = sprint_data[sprint_in_columns]
        train_t = sprint_data[result_column]
        clf.fit(train_d, train_t)
        return clf

    def get_prediction(self, clf, test_data):
        return clf.predict(test_data)

    def get_prediction_last_sprint(self, clf, team, test_data):
        sprint_list = self.data_loader.get_sprint_list(team)
        last_sprint = sprint_list[-1]
        sprint_data = self.data_loader.get_team_sprint_data(team, last_sprint)
        test_d = sprint_data[data_in_columns]
        prediction = self.get_prediction(clf, test_d)
        result = {'data': sprint_data, 'test_d': test_d, 'prediction': prediction}
        return result

    def test_last_sprint(self, clf, team):
        sprint_list = self.data_loader.get_sprint_list(team)
        last_sprint = sprint_list[-1]
        sprint_data = self.data_loader.get_team_sprint_data(team, last_sprint)
        test_d = sprint_data[data_in_columns]
        test_t = sprint_data[result_column]
        data = []
        for i in range(len(test_t)):
            data.append(1)
        prediction = self.get_prediction(clf, test_d)
        r_2 = clf.score(test_d, data)
        p = mean_squared_error(prediction, data)
        result = {'data': sprint_data, 'test_d': test_d, 'test_t': data, 'prediction': prediction, 'r_2': r_2, 'p': p}
        return result

    def test_random_sprint(self, clf, team, omit_last_sprint=True):
        sprint_list = self.data_loader.get_sprint_list(team)
        sprint_list_test = sprint_list
        if omit_last_sprint:
            sprint_list = self.data_loader.get_sprint_list(team)
            sprint_list_test = sprint_list[:-1]
        random_sprint = random.choice(sprint_list_test)
        sprint_data = self.data_loader.get_team_sprint_data(team, random_sprint)
        test_d = sprint_data[data_in_columns]
        test_t = sprint_data[result_column]
        prediction = self.get_prediction(clf, test_d)
        r_2 = clf.score(test_d, test_t)
        p = mean_squared_error(prediction, test_t)
        result = {'data': sprint_data, 'test_d': test_d, 'test_t': test_t, 'prediction': prediction, 'r_2': r_2, 'p': p}
        return result


def intoSingleClass(df, col, target, options):
    tmp = df[df[col] == target]
    for op in options:
        tmp = tmp.append(df[df[col] == op])
    for i in tmp.index:
        df.loc[i, col] = target


def turnClassToNum(df, col):
    elements = df[col].value_counts()
    for i in range(len(elements.index)):
        intoSingleClass(df, col, i + 1, [elements.index[i]])


if __name__ == "__main__":
    pred = Predictor()
    pred.load_data("data.xlsx")
    team = "Greatcall"
    clf_team = pred.train_team(team, omit_last_sprint=True)
    test_result = pred.test_random_sprint(clf_team, team)
    print("Team = " + team)
    print("r_2 = " + str(test_result['r_2']))
    print("p = " + str(test_result['p']))
    test_d = test_result['test_d']
    test_t = test_result['test_t']
    test_result = pred.test_last_sprint(clf_team, team)
    prediction = test_result['prediction']
    fig, ax = plt.subplots()
    ax.scatter(range(len(prediction)), prediction)
    #ax.scatter(range(len(test_t)), test_t.values, label='Real')
    ax.legend()
    plt.show()
