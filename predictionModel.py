# -*- coding: utf-8 -*-
"""
Created on Sun Oct 22 19:10:02 2017

@author: Daniel
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from sklearn import linear_model
import joblib
from tkinter import *
import sqlite3


column_names = ['sprint', 'duration', 'team', 'name', 'seniority', 'val_seniority', 'ue1', 'ue2', 'ue3', 'ue5', 'ue8', 'ue13', 'ce', 'ua1', 'ua2', 'ua3', 'ua5', 'ua8', 'ua13', 'ca', 'e']

def intoSingleClass(df, col, target, options):
    tmp = df[df[col] == target]
    for op in options:
        tmp = tmp.append(df[df[col] == op])
    for i in tmp.index:
        df.loc[i, col] = target


def turnClassToNum(df, col):
    elements = df[col].value_counts()
    for i in range(len(elements.index)):
        intoSingleClass(df, col, i + 1, [elements.index[i]])


data = pd.read_excel('data.xlsx', 'Greatcall', na_values=[0], names=column_names)
#print(data)

data = data[list(data)[0:]]

# data = data[data['Nombre'] == 'Manuel Acuna']
# data = data[data['sprint'] == 2]
trainS = 100  # Número de registros para entrenar el modelo predictor
# testS = 24 #Número de registros para evaluar el rendimiento del modelo predictor

#print(data)
turnClassToNum(data, 'name')
#print(data)
turnClassToNum(data, 'seniority')
#print(data)

data = data.fillna(value=0)
#print(data)

# shuff = data.sample(frac=1)
shuff = data[:]
#print("SHUFF = ")
#print(shuff)

dataIN = shuff[['val_seniority', 'ue1', 'ue2', 'ue3', 'ue5',
                'ue8', 'ue13', 'ce']]
print(dataIN)
trainD = dataIN[:trainS]
print("#### TrainD ####")
print(trainD)

trainT = shuff['e']

# trainT = trainT[:trainS] #comentar cuando se analisa una persona individualmente
trainT = trainT[:]  # comentar para analizar a todo el equipo usando relación train/test

# testD = dataIN[trainS:] #comentar cuando se analisa una persona individualmente
testD = dataIN[:]  # comentar para analizar a todo el equipo usando relación train/test

testT = shuff['e']

# testT = testT[trainS:] #comentar cuando se analisa una persona individualmente
testT = testT[:]  # comentar para analizar a todo el equipo usando relación train/test

# clf = svm.SVR(kernel='rbf', C=1e3, gamma=0.1)
clf = RandomForestRegressor(max_depth=5, random_state=0)
# clf = linear_model.LinearRegression()
## use different parameters for the random forest regressor

clf.fit(trainD, trainT)
## save this object, this is the training
# joblib.dump(clf, "train.data") # Save the trained data
# clf = joblib.load("train.data") # Load the trained data

r_2 = clf.score(testD, testT)
print("r_2 = ")
print(r_2)
#print("testD = ")
#print(testD)
prediction = clf.predict(testD)
#print("prediction = ")
#print(prediction)
p = mean_squared_error(prediction, testT)
print("p = ")
print(p)
## these are the metrics
## using clf.predict with the data that we want to evaluate

fig, ax = plt.subplots()
ax.scatter(range(len(prediction)), prediction, label='Prediction')
ax.scatter(range(len(testT)), testT.values, label='Real')
#ax.scatter(range(len(testD)), testD['ce'], label='Test Data')

ax.legend()

plt.show()
